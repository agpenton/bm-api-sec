# Repository of Security Compliances for InSpec. #

This README would normally document whatever steps are necessary to get your application up and running.

### Security Compliances for running InSpec in bm-api. ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* [Asdrubal Gonzalez](mailto:admin@studio-one.am?Subject=bm-api%20InSpec%20Profile)


#### Summary of set up ####
Install [InSpec](https://downloads.chef.io/inspec/4.18.100) from chef site.

#### Configuration ####
Create a ssh keys and add them to the host that you want to check or pass the password in the command line.

`ssh-keygen`  with this command you generate a pairs of keys.

`ssh-copy-id user@host -p port` copy the public key to the remote host.

#### Dependencies ####
No dependencies at this time.

#### How to run tests ####
`inspec exec https://agpenton@bitbucket.org/agpenton/bm-api-sec.git -t ssh://user@host -i $(private_ssh_key)`


