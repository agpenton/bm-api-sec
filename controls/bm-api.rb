# copyright: 2018, Asdrubal Gonzalez Penton

title "bm-api container section"

describe file("/tmp") do
  it { should be_directory }
end

describe port(5001) do
  its('processes') { should cmp ['docker-proxy'] }
  it { should be_listening }
  its('protocols') { should include 'tcp' }
  its('addresses') { should include '0.0.0.0' }
end

control "bm-api-01" do
  impact 1.0
  title "check status of the Service"
  desc "Check status of the Service"
  describe docker.containers.where { names == 'bm-api' } do
    it { should exist }
    its('status') { should_not eq nil }
    its('images') { should eq ['betmakers/bm-api:qa'] }
    its('ports') { should eq ["0.0.0.0:5001->5001/tcp"] }
    its('names') { should eq ['bm-api']}
    its('networks') { should cmp ["bridge,uaslots_host"]}
  end
end
